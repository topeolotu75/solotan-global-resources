<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('pages.contact');
    }

    public function sendMail(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|digits:11|numeric',
            'subject' => 'required',
            'message' => 'required',
        ]);

        //  Send mail to admin
        \Mail::send('emails.contactMail', array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'subject' => $request->get('subject'),
            'phone' => $request->get('phone'),
            'contact_message' => $request->get('message'),
        ), function($message) use ($request){
            $message->from($request->email);
            $message->to('contact@solotanglobalresources.com', 'Admin')->subject($request->subject);
        });
        toastr()->success('Data has been saved successfully!');
        return redirect()->back();

    }

}
