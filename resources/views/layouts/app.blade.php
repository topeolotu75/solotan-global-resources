<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="painting contractor">
    <meta name="description" content="painting contractor">
    <meta name="author" content="painting contractor">
    <meta name="keywords" content="painting contractor">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$title ?? ''}} | Solotan Global Resources Ltd</title>
    @toastr_css
    <!-- Bootstrap CSS -->
    <link rel="icon" href="{{ $web_source }}/assets/images/logo.png">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="{{ $web_source }}/assets/js/main.js"></script>


     <!-- web fonts -->
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Hind&display=swap" rel="stylesheet">
    <!-- //web fonts -->
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ $web_source }}/assets/css/style-starter.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  </head>
<body>
@include('layouts.includes.header')
@yield('content')
@jquery
@toastr_js
@toastr_render
<div class="elfsight-app-02d8b825-5b2d-4bbb-9f7f-dfc1f9fc400a"></div>
@include('layouts.includes.footer')
@include('layouts.scripts.scripts')

<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="https://apps.elfsight.com/p/platform.js" defer></script>


<script>
  AOS.init();
</script>

</body>
</html>
