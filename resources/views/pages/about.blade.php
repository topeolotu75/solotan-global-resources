@extends('layouts.app',['title' => 'About Us'])
@section('content')
<section class="w3l-about-breadcrum">
  <div class="breadcrum-bg">
    <div class="container py-5">
      <p data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out"><a href="{{url('/')}}">Home</a> &nbsp; / &nbsp; About</p>
      <h2 data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out" class="my-3">About Us</h2>
     <p data-aos="fade-right"  data-aos-duration="3000" data-aos-easing="ease-in-out">We are committed to design and create lively, comfortable, and appealing spaces in homes, apartments, and offices. Everyone at Solotan is willing to go to great lengths to meet customer’s requirements..</p>
    </div>
  </div>
</section>
<!-- content-with-photo4 block -->
<section class="w3l-content-with-photo-4" id="about">
    <div id="content-with-photo4-block" class="py-5">
        <div class="container py-md-3">
            <div class="cwp4-two row">

                <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="cwp4-text col-lg-6">
                        <h3>Company Overview</h3>
                    <p>We offer a wide range of commercial services handled with great care by highly skilled and experienced professionals.
                        We get the job done right with high speed, quality, and precision. We meet the flexible needs of your schedule while minimizing
                        disruption to your business. We provide a smooth, efficient experience from start to finish while handling each project with finesse and expertise at an affordable price.
                        See the below listings for the broad range of commercial painting services offered by SGR.
                    </p>
                    <ul class="cont-4">
                        <li data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>Painting Services</li>
                        <li data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>STUCCO</li>
                        <li data-aos="fade-right"  data-aos-duration="3000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>P.O.P</li>
                        <li data-aos="fade-right"  data-aos-duration="4000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>Wall Screeding</li>
                        <li data-aos="fade-right"  data-aos-duration="5000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>Architectural Design</li>
                        <li data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>General Contractor </li>
                        <li data-aos="fade-right"  data-aos-duration="3000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>Home Improvement Services</li>
                        <li data-aos="fade-right"  data-aos-duration="4000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>Paint Removal</li>
                        <li data-aos="fade-right"  data-aos-duration="5000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>Paint Restoration</li>
                        <li data-aos="fade-right"  data-aos-duration="6000" data-aos-easing="ease-in-out"><span class="mr-3 fa fa-long-arrow-right"></span>Staining</li>

                    </ul>
                    {{-- <a data-aos="fade-up"  data-aos-duration="3000" data-aos-easing="ease-in-out" class="mt-3 btn btn-secondary btn-theme3" href="#team"> About Our Team</a> --}}
                </div>
                <div data-aos="fade-left"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 cwp4-image col-lg-6 pl-lg-5 mt-lg-0">
                    <img src="{{ $web_source }}/assets/images/g4.jpg" class="img-fluid" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>
<!-- content-with-photo4 block -->
<!-- services block1 -->
<section class="w3-about-2" id="services">
<div class="py-5 w3-services-ab">
    <div class="container py-lg-3">
        <div class="mx-auto text-center heading">
            <h3 data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="head">Core Values</h3>
          </div>
        <div class="w3-services-grids">

            <div class="w3-services-right-grid">
              <div class="mt-5 fea-gd-vv row pt-lg-3">
                    <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="col-md-4 col-sm-6">
                        <div class="float-lt feature-gd">
                          <div class="icon"> <span class="fa fa-trophy" aria-hidden="true"></span></div>
                          <div class="icon-info">
                             <h5 data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out">PROFESSIONALISM</h5>
                             <p data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out">
                                Committed to deliver turnkey solutions by the code
                                Passionate for meeting project milestones and client’s budget
                                Capable of managing project to the end
                             </p>

                         </div>
                        </div>
                     </div>
                     <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 col-md-4 col-sm-6 mt-sm-0">
                        <div class="float-mid feature-gd">
                          <div class="icon"> <span class="fa fa-paint-brush" aria-hidden="true"></span></div>
                          <div class="icon-info">
                             <h5 data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out">OUT OF THE BOX DESIGN</h5>
                             <p data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out">
                                A team of forward thinkers among remodeling industry
                                Smart approach to create effective and efficient space designs
                                {{-- Enthusiastic to experiment with latest trends and materials --}}
                             </p>
                            </div>
                         </div>
                  </div>
                  <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 col-md-4 col-sm-6 mt-md-0">
                    <div class="float-mid feature-gd">
                      <div class="icon"> <span class="fa fa-shield " aria-hidden="true"></span></div>
                      <div class="icon-info">
                         <h5 data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out">SAFETY AND SUSTAINABILITY</h5>
                         <p data-aos="fade-right"  data-aos-duration="2000" data-aos-easing="ease-in-out">
                            Prioritizing safe practices over everything else
                            {{-- Striving to reduce the carbon footprint through sustainable practices --}}
                            Ensuring compliance to environmental and safety regulations
                         </p>
                        </div>
                     </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- //services block1 -->
<section class="teams-1">
{{-- <section class="py-5 text-center teams" id="team">
		<div class="container py-xl-5 py-lg-3">
			<div class="mx-auto text-center heading">
				<h3 class="head">Our Experts</h3>
				<p class="my-3 head"> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
				  Nulla mollis dapibus nunc, ut rhoncus
				  turpis sodales quis. Integer sit amet mattis quam.</p>

			  </div>
			<div class="pt-5 mt-3 row inner-sec-w3ls-w3pvt-aminfo">
				<div class="col-lg-3 col-sm-6">
					<div data-aos="fade-down"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="text-center team-grid">
						<div  class="team-img">
							<img class="rounded img-fluid" src="{{ $web_source }}/assets/images/t1.jpg" alt="">
						</div>
						<div class="team-info">
							<h4>Trent Boult</h4>
							<ul class="py-3 d-flex justify-content-center social-icons">
								<li class="effect-soc-team1">
									<a href="#">
										<span class="fa fa-facebook-f"></span>
									</a>
								</li>
								<li class="effect-soc-team2">
									<a href="#">
										<span class="fa fa-twitter"></span>
									</a>
								</li>
								<li class="effect-soc-team3">
									<a href="#">
										<span class="fa fa-google-plus"></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div data-aos="fade-up"  data-aos-duration="2000" data-aos-easing="ease-in-out" class="col-lg-3 col-sm-6 top-tem">
					<div class="text-center team-grid">
						<div class="team-img">
							<img class="rounded img-fluid" src="{{ $web_source }}/assets/images/t2.jpg" alt="">
						</div>
						<div class="team-info">
							<h4>Henry David</h4>
							<ul class="py-3 d-flex justify-content-center social-icons">
								<li class="effect-soc-team1">
									<a href="#">
										<span class="fa fa-facebook-f"></span>
									</a>
								</li>
								<li class="effect-soc-team2">
									<a href="#">
										<span class="fa fa-twitter"></span>
									</a>
								</li>
								<li class="effect-soc-team3">
									<a href="#">
										<span class="fa fa-google-plus"></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div data-aos="fade-down"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 col-lg-3 col-sm-6 mt-sm-0">
					<div class="text-center team-grid">
						<div class="team-img">
							<img class="rounded img-fluid" src="{{ $web_source }}/assets/images/t3.jpg" alt="">
						</div>
						<div class="team-info">
							<h4>Jonty Rhoods</h4>
							<ul class="py-3 d-flex justify-content-center social-icons">
								<li class="effect-soc-team1">
									<a href="#">
										<span class="fa fa-facebook-f"></span>
									</a>
								</li>
								<li class="effect-soc-team2">
									<a href="#">
										<span class="fa fa-twitter"></span>
									</a>
								</li>
								<li class="effect-soc-team3">
									<a href="#">
										<span class="fa fa-google-plus"></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div data-aos="fade-up"  data-aos-duration="2000" data-aos-easing="ease-in-out" class="col-lg-3 col-sm-6 top-tem">
					<div class="text-center team-grid">
						<div class="team-img">
							<img class="rounded img-fluid" src="{{ $web_source }}/assets/images/t4.jpg" alt="">
						</div>
						<div class="team-info">
							<h4>Andrew Tye</h4>
							<ul class="py-3 d-flex justify-content-center social-icons">
								<li class="effect-soc-team1">
									<a href="#">
										<span class="fa fa-facebook-f"></span>
									</a>
								</li>
								<li class="effect-soc-team2">
									<a href="#">
										<span class="fa fa-twitter"></span>
									</a>
								</li>
								<li class="effect-soc-team3">
									<a href="#">
										<span class="fa fa-google-plus"></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
    </section>
</section>


@endsection
