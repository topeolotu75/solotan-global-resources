@extends('layouts.app', ['title' => 'Services'])
@section('content')
<section class="w3l-service-breadcrum">
  <div class="breadcrum-bg">
    <div class="container py-5">
      <p data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out"><a href="/">Home</a> &nbsp; / &nbsp; Services</p>
      <h2 data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="my-3">Our Services</h2>
      <p data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out">We are offering the following information's about us that what we actually do in the Interior and Outerior Painting services sector.</p>
    </div>
  </div>
</section>
 <!--  services section -->
<section class="w3l-index-7" id="service">
  <div class="py-5 features-with-17_sur">
    <div class="container py-lg-3">
      <div class="features-with-17-top_sur">
        <div class="row">
          <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-4 col-lg-4 col-md-6 mt-md-0">
            <div class="features-with-17-right-tp_sur">
              <div class="mb-3 features-with-17-left1">
                <span class="fa fa-paint-brush" aria-hidden="true"></span>
              </div>
              <div class="features-with-17-left2">
                <h6><a href="#url">Exterior Painting</a></h6>
                <p>Solotan team of dependable exterior painting professionals will tackle your list of painting projects. They know how to do the job right
                    with years of experience painting a wide variety of surfaces and materials, including: stucco, wood, vinyl or aluminum siding, brick, cedar shingles, and garage doors.</p>
                 </div>
            </div>
          </div>
          <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 col-lg-4 col-md-6 mt-md-0">
            <div class="features-with-17-right-tp_sur">
              <div class="mb-3 features-with-17-left1">
                <span class="fa fa-paint-brush"" aria-hidden="true"></span>
              </div>
              <div class="features-with-17-left2">
                <h6><a href="#url">Painting and Staining</a></h6>
                <p>
                    At Solotan we offer a variety of interior and exterior home painting and staining services.
                     Regardless of the room inside your home that needs an update or the area of your home’s exterior that needs protection from Mother Nature,
                     our team works with you to determine the best plan of action for your home.
                </p>
              </div>
            </div>
          </div>
          <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 col-lg-4 col-md-6 mt-lg-0">
            <div class="features-with-17-right-tp_sur">
              <div class="mb-3 features-with-17-left1">
                <span class="fa fa-paint-brush"" aria-hidden="true"></span>
              </div>
              <div class="features-with-17-left2">
                <h6><a href="#url">Stucco Repair and Painting</a> </h6>
                <p>
                    As with any other home exterior, it’s important to maintain the stucco or EIFS
                    covering your home. The Solotan team  is very knowledgeable in both repairs and painting related
                     to these two materials and will work alongside you to determine what services your home needs.
                </p>
              </div>
            </div>
          </div>
          <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 col-lg-4 col-md-6">
            <div class="features-with-17-right-tp_sur">
              <div class="mb-3 features-with-17-left1">
                <span class="fa fa-paint-brush"" aria-hidden="true"></span>
              </div>
              <div class="features-with-17-left2">
                <h6><a href="#url">Power Washing</a> </h6>
                <p>
                    Power washing is a vital step in painting the exterior of your home.
                    Solotan uses the process to remove mold, oil and old paint so new paint absorbs into your surface better.
                </p>
              </div>
            </div>
          </div>
          <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 col-lg-4 col-md-6">
            <div class="features-with-17-right-tp_sur">
              <div class="mb-3 features-with-17-left1">
                <span class="fa fa-paint-brush"" aria-hidden="true"></span>
              </div>
              <div class="features-with-17-left2">
                <h6><a href="#url">Vinyl Siding Painting</a> </h6>
                <p>Whether you have high-tech vinyl siding or a more traditional siding,
                     Solotan knows how to restore them. Our team has the right approach to add new life to your exteriors.</p>
                  </div>
            </div>
          </div>
          <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mt-5 col-lg-4 col-md-6">
            <div class="features-with-17-right-tp_sur">
              <div class="mb-3 features-with-17-left1">
                <span class="fa fa-paint-brush"" aria-hidden="true"></span>
              </div>
              <div class="features-with-17-left2">
                <h6><a href="#url">
                    Interior Painting</a> </h6>
                <p>When it comes to painting your house interiors, you need professional results. Your home deserves a high-quality service and an impeccable finish. Our interior house painting services provide a seamless, and efficient, and meticulous interior painting job.</p>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 <!--  //services section -->
<!-- //services block1 -->
<!-- services block3 -->
<section class="w3l-service-3" id="features">
<div class="py-5 w3l-open-block-services">
    <div class="container py-lg-3">

        <div class="mx-auto text-center heading">
            <h3 data-aos="fade-up"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="head">Our Best Features</h3>
            <p data-aos="fade-up"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="my-3 head ">
                We wants you to have the best possible experience along the way. Whether your project is for interior or exterior painting, one room or your entire home, your satisfaction is what matters most.
                From start to finish, you can depend on us to deliver on our promise of professionalism, care and quality.
            </p>
          </div>
        <div class="pt-3 mt-5 row">
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <h4 data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mission">Respect</h4>
                    <div class="icon-holder">
                        <span data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="fa fa-handshake-o" aria-hidden="true"></span>
                    </div>
                        <div data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="open-description">
                        <p>We respect your schedule by arriving on time and completing our work efficiently and neatly.</p>

                    </div>
                </div>
            </div>
            <div class="pt-3 mt-5 col-lg-4 col-md-6 mt-md-0 pt-md-0">
                <div class="card">

                    <h4 data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mission">Regular Communication</h4>
                    <div class="icon-holder">
                        <span data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="fa  fa-comments" aria-hidden="true"></span>
                    </div>
                    <div class="open-description">
                        <p data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out">We provide you with daily updates the way you want them: via text, email, phone, or in person.</p>
                    </div>
                </div>
            </div>
            <div class="pt-3 mt-5 col-lg-4 col-md-6 mt-lg-0 pt-lg-0">
                <div class="card">

                    <h4 data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="mission">Professional Services</h4>
                    <div class="icon-holder">
                        <span data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out" class="fa fa-wrench" aria-hidden="true"></span>
                    </div>
                    <div class="open-description">
                        <p data-aos="fade-right"  data-aos-duration="1000" data-aos-easing="ease-in-out">We treat your property as if it were our own, following a process that ensures a beautiful, professional result that you’ll love.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
</section>

@endsection
